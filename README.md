# Chemins

[![Chemins demo](/screenshots/main.png)](https://vimeo.com/288141552 "Chemins demo")

## About Chemins

Chemins is a software for interactive narrations that can be used as a writing tool and/or website administration interface. It is built on top of the CMS and framework ProcessWire. See [Processwire website](https://processwire.com). It's developped with html/css/js for the graphical interface and PHP for the communication with ProcessWire. It can be used for creating complex networks where each "question" node is a page (on the public website) and each "answer" node is a link to another "question" node (thus a page). More information about Chemins can be found [on this page](https://lionelmaes.com/chemins/). For the moment, Chemins is more a (usable) work in progress/proof of concept than a "mature" software, meaning that you will probably have to modify the templates files (inside the /site folder) and make some adjustments inside the ProcessWire administration interface (depending of your usage).

## Installing Chemins

* Extract all the files to an http accessible location. 
* Import the "chemins.sql" file into your MySQL or MariaDB database.
* Modify the "config.php" file (/site/config.php) with the correct information (Database Configuration and HTTP Hosts Whitelist sections).

## Configuring Chemins

![ProcessWire admin](/screenshots/screenshot2.png)

* First login into the ProcessWire administration by going to the URL where you installed Chemins followed by "/ctrl" (eg. http://yoursite.com/ctrl). The default user name is "demo", the password is "cheminsdemo1".
* The first thing you should do is to modify the user "demo" by going to "access/users" inside the administration interface. Replace the login, email and password with your own.
* Back to the homepage of the administration interface, you will see one page named "DEMO" with 4 subpages; "Questions", "Answers", "Structure" and "Sessions". This is an example of an instance of Chemins. If you need to make more than one instance inside your website, you just have to duplicate this structure.

## Using Chemins

### Accessing the Chemins writing tool

* The three interfaces that are part of Chemins are the ProcessWire administration interface, the public website and the Chemins writing tool. This tool is accessible by clicking on the "view" button next to the "Structure" page in the administration interface or by going directly to the url of the Chemins instance followed by "/structure" (eg. http://yoursite.com/demo/structure).
* Every changes made into the graph is saved into the ProcessWire administration, when clicking on "save" inside the writing tool.

### Making a node in the graph

![Making a node](/screenshots/1-box.gif)

* Double click anywhere in the webpage, it creates a box. Click on a created box, it becomes selected. You can type anything inside a box when it's selected.
* Clicking on the interrogation mark/exclamation mark on the top left of a box changes its status. A box can either be a question or an answer.
* To move a box, simply drag and drop it where you want to. You can also move a box and all its descendents by maintaining "ctrl" key before dragging and dropping it.
* To delete a box, click on the black arrow on its top right corner.

### Linking nodes

![Linking nodes](/screenshots/2-edges.gif)

* The rules for connecting boxes are: a question node can be connected to one or several answer nodes, an answer node can be connected to only one question node. You cannot connect one question to another question nor one answer to another answer. Those rules are explained by the "navigational logic" of Chemins: the questions are "places" (webpages in the public website) and the answers are path (links in the public website) that one can take to go from one question to another.
* To connect two boxes, click on an input connector (the black arrow on top of the box), then click on an output connector (the black arrow at the bottom of another box). You can also click first on an output connector, then on an input connector.
* To remove a link between two boxes, click on it.

### Recording and exporting visitors sessions

![Recording and exporting](/screenshots/3-export.gif)

* If you want to record the visitors sessions in the public website, you have to set the boxes you want monitored as "watched" by clicking on the eye on the top right of the boxes.
* You can access the sessions by switching the mode to "data" (click on write) on top of the page, select the boxes you want to export and click "export".

  





