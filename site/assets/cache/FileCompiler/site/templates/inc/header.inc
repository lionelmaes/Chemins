<?php

 include(\ProcessWire\wire('files')->compile(\ProcessWire\wire("config")->paths->root . 'site/templates/inc/functions.inc',array('includes'=>true,'namespace'=>true,'modules'=>true,'skipIfNamespace'=>true)));

    $session->prev = $page->id;



?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'>
		<title><?php echo $page->title; ?></title>
        <link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>styles/normalize.css" />
		<link rel="stylesheet" type="text/css" href="https://meyerweb.com/eric/tools/css/reset/reset.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>styles/fonts.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>styles/main.css" />

        <meta property="og:url" content="<?php echo $page->httpUrl; ?>" />
        <meta property="og:title" content="<?php echo $page->title; ?>" />
        <meta property="og:description" content="<?php echo strip_tags(($page->text)?$page->text:$page->title); ?>" />
        <script src="<?php echo $config->urls->templates?>scripts/jquery-3.1.1.min.js" type="text/javascript"></script>
        <script src="<?php echo $config->urls->templates?>scripts/raphael.min.js" type="text/javascript"></script>

	</head>
	<body>
        <main>
