
<?php

function getTextfieldAnswer($answer, $session){
    foreach($session->textdatas as $data){
        if($data->answer == $answer){
            return $data->textdata;
        }
    }
    return false;
}


function replaceCSVHeaders($fp, $file, $oldLineLength, $newHeaders){
    fseek($fp, $oldLineLengh);
    ftruncate($fp, filesize($file) - $oldLineLength);
    rewind($fp);
    fputcsv($fp, $newHeaders);

}

if(!$user->isSuperuser()){
    header('Location: /');
    exit();

}

if($config->ajax):


    $params = json_decode(trim(file_get_contents('php://input')), true);

    if(isset($params['action']) && $params['action'] == 'delete'):
        $sessions = $page->children('template=session');
        foreach($sessions as $session){
            $session->delete();

        }
        $output = array('result'=>'ok', 'msg'=>'sessions deleted');
        echo json_encode($output);

    elseif(isset($params['action']) && $params['action'] == 'buildcsv'):

        $questions = $pages->find('template=question,watched=1');
        $headers = array();
        foreach($questions as $question){
            $headers[] = '#'.$question->id.'#'.$sanitizer->unentities($question->title);
        }

        $fp = fopen('./export.csv', 'w');
        fputcsv($fp, $headers);
        fclose($fp);

        $output = array('result'=>'ok', 'msg'=>'CSV header built. Let\'s fill the file, shall we?');
        echo json_encode($output);

    elseif(isset($params['action']) && $params['action'] == 'fillcsv'):

        //on load le csv pour le modifier
        $fp = fopen('./export.csv', 'a+');

        $headers = fgetcsv($fp);



        $sessions = $pages->find('template=session,start='.$params['start'].',limit='.$params['limit']);
        if(count($sessions) == 0){
            $output = array('result'=>'ok', 'msg'=>'end');

        }
        else{
            $output = array('result'=>'ok', 'msg'=>'next');
        }
        foreach($sessions as $session):
            $outputLine = array();

            foreach($headers as $header){
                preg_match_all('!#([0-9]+)#!', $header, $matches);
                $idQ = $matches[1][0];
                preg_match_all('!'.$idQ.'>([0-9]+)!', $session->trail, $matches);
                if(count($matches[1]) == 0){
                    $aText = '';
                }else{
                    $idA = $matches[1][0];
                    $answer = $pages->get($idA);
                    if($answer->textfield === 1)
                        $aText = $sanitizer->unentities(getTextfieldAnswer($answer, $session));
                    else
                        $aText = $sanitizer->unentities($answer->title);

                }
                $outputLine[] = $aText;


            }
            fputcsv($fp, $outputLine);

        endforeach;

        fclose($fp);
        echo json_encode($output);

    endif;



else:
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<title><?php echo $page->title; ?></title>
        <link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>styles/normalize.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>styles/fonts.css" />
        <script src="<?php echo $config->urls->templates?>scripts/jquery-3.1.1.min.js" type="text/javascript"></script>
        <style>

            main{
                margin-left:3rem;

            }
            section.actions p{
                color:blue;
                text-transform:uppercase;
                cursor:pointer;
            }
        </style>
	</head>
	<body>


    <main>
        <section class="actions">
            <p data-action="start">Click here to start</p>
            <p data-action="buildcsv">or Build CSV header</p>
            <p data-action="fillcsv">or Fill CSV with data</p>
            <a href="/site/templates/export.csv">Or download the current export</a>
            <p data-action="deleteall">Or remove all the sessions (warning: potential data loss!)</p>
        </section>
        <section class="log">

        </section>
    </main>


	<script src="<?php echo $config->urls->templates?>scripts/sessions.js" type="text/javascript"></script>
	</body>
</html>
<?php
endif;
 ?>
