<?php

    if(!isset($question)){
        $question = $page;
    }

    if(!$config->ajax){
        include("./inc/header.inc");
    }
    else{
       include("./inc/functions.inc");
    }




    $answers = $question->link_answer;

    if($question->pageType != '') {
        $pageType = $question->pageType->title;
    } else {
        $pageType = "question";
    }
?>

<section data-url="<?php echo (isset($pageUrl))?$pageUrl:$page->url; ?>" class="page <?php echo ($question->watched == 1)?' watched ':''; echo ($question->home == 1)?' home ':''; echo "page-" . $pageType; ?>" <?php echo ($question->menuON == 1)?' data-menu="true"':''?>>

        <!-- Q U E S T I O N -->
        <section class="question" data-url="<?php echo $question->url;?>">

                <h2><?php echo $question->title; ?></h2>

<?php
    if($question->text != ""):
?>
                <div class="detail"><?php echo $question->text; ?></div>
<?php
    endif;
?>
        
        </section>



<?php
        #--- A N S W E R S ---#
        if(count($answers) > 0):
?>
        <section class="answers">


<?php
            foreach($answers as $answer):
                $question = $answer->link_question;

?>


            <div class="<?php echo (($answer->focus)?' focus':'').((isset($answer->pageType) && $answer->pageType->title != '')?' '.$answer->pageType->title:' answer');?>" data-url="<?php echo $answer->url;?>" <?php echo (($answer->textfield_nbchars)?'data-maxlength=' . $answer->textfield_nbchars:'');?>>
<?php

                $answer->text = dataProcess($answer->text);
                //1: une réponse formulaire
                if($answer->textfield === 1): //avec du texte
                    if(preg_match('#[0-9a-z]+#i', $answer->title)):
?>
                    <div class="bubble-content">
                        <h3><?php echo $answer->title; ?></h3>
                        <div class="detail">

                        <?php echo $answer->text; ?>
                        </div>
                    </div>


<?php

                    endif;
?>

                <div class="answer--form">

                    <form method="post" action="<?php echo $answer->url; ?>">
                    <textarea style="<?php echo (($answer->textfield_nbchars)?' width:' . ($answer->textfield_nbchars * 2) . 'px;':'width: 23vw;');?>"width: <?php echo ($answer->textfield_nbchars * 2) . 'px' ?>" placeholder="ici" name="data" <?php echo (($answer->textfield_nbchars)?'maxlength=' . $answer->textfield_nbchars:'');?>></textarea>
                        <input type="submit" value="OK">
                    </form>

                </div>

<?php
                //2: une réponse normale
                else:

?>
                <div class="bubble-content">
<?php
                if($question):

?>
                <a class="navig-link" href="<?php echo $answer->url; ?>">
<?php
                endif;
?>
                <h3><?php echo $answer->title; ?></h3>
                <div class="detail">

                    <?php echo $answer->text; ?>
                </div>
<?php
                if($question):
?>
                </a>
<?php
                endif;
?>

                </div>

<?php
                endif;
?>



            </div>
<?php
            endforeach;
?>




        </section> <!-- END .answers -->
<?php
        endif;
?>





</section> <!-- END .page -->




<?php
    if(!$config->ajax){
        include("./inc/footer.inc");
    }
?>
